const api = function getApi(sign, day) {
    return `https://aztro.sameerkumar.website/?sign=${sign}&day=${day}`
};
const queries = {
    sign: "aries",
    day: "today"
};

const loader = document.getElementById('loader');
const container = document.getElementById('root');
const selector = document.getElementById('selection');
let error = false;
let info = {};

async function getInfo(params) {
    loader.style.display = 'block';
    const response = await fetch(api(params.sign, params.day), {
        method: 'POST'
    })
    if (response.ok) {
        const data = await response.json();
        loader.style.display = 'none';
        info = data;
        convertHtml(info);
    } else {
        loader.style.display = 'none';
        error = true;
    }
}

document.addEventListener("DOMContentLoaded", () => {
    let sign;
    chrome.storage.sync.get('sign', function (result) {
        console.log(result);
        sign = result.sign;
        if (sign) {
            queries.sign = sign;
        } else {
            queries.sign = "aries";
        };
        selector.value = queries.sign;
        getInfo(queries);
    });
})

if (selector) {
    selector.addEventListener('change', (e) => {
        queries.sign = e.target.value;
        chrome.storage.sync.set({ 'sign': e.target.value }, function () {
            console.log(e.target.value);
        });
        getInfo(queries);
    });
}

function convertHtml(obj) {
    container.innerHTML = `
        <div><span>Description </span> - ${obj.description}</div>
        <div><span>Compatibility </span> - ${obj.compatibility}</div>
        <div><span>Lucky Time </span> - ${obj.lucky_time}</div>
        <div><span>Lucky Number </span> - ${obj.lucky_number}</div>
        <div><span>Color </span> - ${obj.color}</div>
        <div><span>Mood </span> - ${obj.mood}</div>
    `
}














